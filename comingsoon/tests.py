from django.test import TestCase
from comingsoon.views import get_date
import datetime


class GetDateTestCase(TestCase):
    def test_date_is_right(self):
        date = get_date()
        self.assertEqual(date, (datetime.datetime(2024, 9, 1) - datetime.datetime.now()).days)
