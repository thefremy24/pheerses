from django.shortcuts import render
import datetime

def get_date():
    now = datetime.datetime.now()
    print(now.year)
    if now >= datetime.datetime(now.year, 9, 1):
        return (datetime.datetime(now.year+1, 9, 1) - now).days
    return (datetime.datetime(now.year, 9, 1) - now).days


def home(request):
    context = {'date': get_date()}
    template = 'comingsoon/home.html'
    return render(request, template, context)
